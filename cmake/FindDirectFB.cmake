include(FindPackageHandleStandardArgs)

find_path(DIRECTFB_H_PATH NAMES directfb.h PATH_SUFFIXES directfb)
find_library(DIRECTFB_LIB NAMES directfb)

find_package_handle_standard_args(DIRECTFB DEFAULT_MSG DIRECTFB_LIB DIRECTFB_H_PATH)
mark_as_advanced(DIRECTFB_H_PATH DIRECTFB_LIB)

# set up output vars

if (DIRECTFB_FOUND)
    set(DIRECTFB_INCLUDE_DIR ${DIRECTFB_H_PATH})
    set(DIRECTFB_LIBRARIES ${DIRECTFB_LIB})
else()
    set(DIRECTFB_INCLUDE_DIR)
    set(DIRECTFB_LIBRARIES)
endif()
