/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#ifndef MESSAGE_H
#define MESSAGE_H

#include "types.h"

#include <vector>

// represents a serialized message, for internal use only... unless you are really creative.
class Message
{
public:
    Message();
    // reserve space for message header and fill it in (except for length)
    void beginLoad(int objectId, int methodId);
    // add an argument - use this between beginLoad() and endLoad() calls
    template<typename T> void append(T arg);
    // fill in the length header, finishing the message
    void endLoad();

    // call this before take()ing any arguments
    void beginUnload();
    // remove and return an argument, this is first-in-first out with append()
    template<typename T> T take();
    // call this after take()ing all arguments: this checks that the message's length was correct
    void endUnload();


    // add a file descriptor to the message, to be passed using a magic local socket mechanism.
    // the file descriptor will generally be numerically different on the other side, but access
    // the same file or other file-like object (like shared memory).
    void appendFileDescriptor(int fd);

    // return nth file descriptor that was added using addFileDescriptor()
    int fileDescriptor(int index) const;

    int length() const { return m_bufferFill; }
    int objectId() const { return reinterpret_cast<const int *>(m_buffer)[0]; }
    int methodId() const { return reinterpret_cast<const int *>(m_buffer)[1] & 0xffff; }

    // special message IDs and first class-specific ID
    enum MessageIds
    {
        DetachMethodId = 0,
        ChokeMethodId,
        FirstUserMethodId = 10
    };

    // at least we're not exposing the data as writable... still kinda ugly.
    const char *data() const { return m_buffer; }
private:
    friend class Connection; // expose the data buffers and header size
    static const int m_headerSize = 2 * sizeof(int);

    void addData(const void *buf, int length);

    // read and write buffers; messages larger than bufsize are not allowed for now
    static const int m_bufSize = 65536;
    static const int m_maxTransferFds = 32; // maximum number of file descriptor arguments per method call

    char m_buffer[m_bufSize];
    int m_bufferFill;
    char *m_unloadPos;
    std::vector<int> m_fileDescriptors;
};

class RemoteObject;

template<> void Message::append<int>(int i);
template<> void Message::append<uint>(uint i);
template<> void Message::append<array>(array a);
template<> void Message::append<const char *>(const char *s);
template<> void Message::append<const RemoteObject *>(const RemoteObject *obj);

template<> int Message::take();
template<> uint Message::take();
template<> array Message::take();
template<> const char *Message::take();
// template<> RemoteObject *Message::take(); // this is less awkward to do directly in the generated code

#endif // MESSAGE_H