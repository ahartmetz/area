#!/usr/bin/python

#   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Library General Public
#   License as published by the Free Software Foundation; either
#   version 2 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Library General Public License for more details.
#
#   You should have received a copy of the GNU Library General Public License
#   along with this library; see the file COPYING.LGPL.  If not, write to
#   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
#   Alternatively, this file is available under the Mozilla Public License
#   Version 1.1.  You may obtain a copy of the License at
#   http://www.mozilla.org/MPL/

# We need to generate four files, one header and one implementation each for client and server.
# They will be called clientsideprotocol.cpp/h and serversideprotocol.cpp/h for the client and
# server side code, respectively.
# Protocol handling will be implemented in the .cpp files; code *using* the protocol handling code
# should derive from classes in the .h files.

## .h file

headerFileTemplate = "\
#ifndef $_guardDefine_$ \n\
#define $_guardDefine_$\n\
\n\
#include \"connection.h\"\n\
#include \"remoteobject.h\"\n\
$_classDeclarations_$\n\
#endif // $_guardDefine_$\n"

classDeclarationTemplate = "\n\
class $_className_$ : public RemoteObject\n\
{\n\
public:\n\
    $_className_$(IConnection *connection, int id = 0)\n\
        : RemoteObject(connection, id, \"$_className_$\")\n\
    {}\n\
\n\
$_sendMethodDeclarations_$\
\n\
$_receiveMethodDeclarations_$\
\n\
    void dispatch(Message &message);\n\
};\n"

sendMethodDeclarationTemplate = "\
    void send_$_methodName_$($_parameterList_$);\n"

receiveMethodDeclarationTemplate = "\
    virtual void receive_$_methodName_$($_parameterList_$) = 0;\n"

## .cpp file

implFileTemplate = "\
#include \"$_headerFileName_$\"\n\
\n\
#include <cassert>\n\
$_classMethods_$"

classMethodsTemplate = "\
$_sendMethods_$\
$_dispatchMethod_$"

sendMethodTemplate = "\n\
void $_className_$::send_$_methodName_$($_parameterList_$)\n\
{\n\
    Message m;\n\
    m.beginLoad(objectId(), $_messageId_$);\n\
$_parameterSerializations_$\
    m.endLoad();\n\
    connection()->send(m);\n\
}\n"

parameterSerializationTemplate = "\
    m.append($_parameterName_$);\n"

fdSerializationTemplate = "\
    m.appendFileDescriptor($_parameterName_$);\n"

dispatchMethodTemplate = "\n\
void $_className_$::dispatch(Message &message)\n\
{\n\
    message.beginUnload();\n\
    switch (message.methodId()) {\n\
$_dispatchCases_$\
    default:\n\
        assert(false);\n\
    }\n\
}\n"

dispatchCaseTemplate = "\
    case $_methodId_$: {\n\
$_parameterDeserializations_$\
        message.endUnload();\n\
        receive_$_methodName_$($_parameterList_$);\n\
        break; }\n"

parameterDeserializationTemplate = "\
        $_parameterType_$ $_parameterName_$ = message.take<$_parameterType_$>();\n"

stringDeserializationTemplate = "\
        const char *$_parameterName_$ = message.take<const char *>();\n"

objectDeserializationTemplate = "\
        int $_parameterName_$_id = message.take<int>();\n\
        RemoteObject *$_parameterName_$ = connection()->objectFromId($_parameterName_$_id);\n"

#### HACK this can only work with one file descriptor per message for now
fdDeserializationTemplate = "\
        int $_parameterName_$ = message.fileDescriptor(0);\n"

new_idDeserializationTemplate = "\
        int $_parameterName_$ = message.take<int>();\n"

## parsing code

import xml.dom.minidom
import sys

try:
    doc = xml.dom.minidom.parse(sys.argv[1])
except:
    print 'Could not open input file wayland.xml!'
    sys.exit(1)

class Function:
    def __init__(self, name):
        self.name = name
        self.arguments = [] # format: [('int', 'width'), ('int', 'height')]
    def formatArguments(self, longForm = True):
        s = ''
        for a in self.arguments:
            if longForm:
                ty = a[0] + ' '
                if ty == 'string ':
                    ty = 'const char *'
                elif ty == 'object ':
                    ty = 'const RemoteObject *'
                elif ty == 'fd ' or ty == 'new_id ':
                    ty = 'int '
                s += ty
            s += a[1] + ', '
        return s[:-2]

class Interface:
    def __init__(self, name):
        self.name = name
        self.requests = [] # Function objects
        self.events = [] # Function objects

interfaces = [] # list of Interface instances, input to code generation stage

def handleProtocol(doc):
    protocol = doc.getElementsByTagName("protocol")[0]
    for iface in protocol.getElementsByTagName("interface"):
        handleInterface(iface)

def handleInterface(iface):
    i = Interface(iface.attributes["name"].value)
    for req in iface.getElementsByTagName("request"):
        i.requests.append(handleFunction(req))
    for evt in iface.getElementsByTagName("event"):
        i.events.append(handleFunction(evt))
    interfaces.append(i)


def handleFunction(func):
    f = Function(func.attributes["name"].value)
    for arg in func.getElementsByTagName("arg"):
        f.arguments.append((arg.attributes["type"].value, arg.attributes["name"].value))
    return f

handleProtocol(doc)

## code generation code

if False:
    #debug code
    for i in interfaces:
        print '** ' + i.name + ' **'
        for r in i.requests:
            print 'R ' + r.name + '(' + r.formatArguments() + ')'
        for e in i.events:
            print 'E ' + e.name  + '(' + e.formatArguments() + ')'
        print ''

def cleanEmptyLines(s):
    return s.replace('\n\n\n', '\n\n').replace('\n\n\n', '\n\n').replace('{\n\n', '{\n').replace('\n\n}', '\n}')

def generateHeaders(f, isServerSide = False):
    classDeclarations = ''
    for iface in interfaces:
        # a class declaration for each interface
        if isServerSide:
            requests = iface.events
            events = iface.requests
        else:
            requests = iface.requests
            events = iface.events

        sendMethodDeclarations = ''
        for req in requests:
            sendMethodDeclarations += sendMethodDeclarationTemplate.replace('$_methodName_$', req.name) \
                                      .replace('$_parameterList_$', req.formatArguments())
        receiveMethodDeclarations = ''
        for evt in events:
            receiveMethodDeclarations += receiveMethodDeclarationTemplate.replace('$_methodName_$', evt.name) \
                                         .replace('$_parameterList_$', evt.formatArguments())

        classDeclarations += classDeclarationTemplate.replace('$_className_$', iface.name) \
                             .replace('$_sendMethodDeclarations_$', sendMethodDeclarations) \
                             .replace('$_receiveMethodDeclarations_$', receiveMethodDeclarations)

    guardDefine = f[:-2].upper() + '_H'
    headerFile = headerFileTemplate.replace('$_classDeclarations_$', classDeclarations) \
                 .replace('$_guardDefine_$', guardDefine)
    open(f, 'w').write(cleanEmptyLines(headerFile))

def generateImpls(f, isServerSide = False):
    classMethods = ''
    for iface in interfaces:
        if isServerSide:
            requests = iface.events
            events = iface.requests
        else:
            requests = iface.requests
            events = iface.events

        sendMethods = ''
        methodId = 10
        for req in requests:
            parameterSerializations = ''
            for arg in req.arguments:
                if arg[0] == 'fd':
                    parameterSerializations += fdSerializationTemplate.replace('$_parameterName_$', arg[1])
                else:
                    parameterSerializations += parameterSerializationTemplate.replace('$_parameterName_$', arg[1])

            sendMethods += sendMethodTemplate.replace('$_className_$', iface.name) \
                           .replace('$_methodName_$', req.name) \
                           .replace('$_parameterList_$', req.formatArguments()) \
                           .replace('$_messageId_$', str(methodId)) \
                           .replace('$_parameterSerializations_$', parameterSerializations)
            methodId += 1

        dispatchCases = ''
        methodId = 10
        for evt in events:
            parameterDeserializations = ''
            for arg in evt.arguments:
                if arg[0] == 'string':
                    parameterDeserializations += stringDeserializationTemplate.replace('$_parameterName_$', arg[1])
                elif arg[0] == 'object':
                    parameterDeserializations += objectDeserializationTemplate.replace('$_parameterName_$', arg[1])
                elif arg[0] == 'fd':
                    parameterDeserializations += fdDeserializationTemplate.replace('$_parameterName_$', arg[1])
                elif arg[0] == 'new_id':
                    parameterDeserializations += new_idDeserializationTemplate.replace('$_parameterName_$', arg[1])
                else:
                    parameterDeserializations += parameterDeserializationTemplate.replace('$_parameterType_$', arg[0]) \
                                                 .replace('$_parameterName_$', arg[1])

            dispatchCases += dispatchCaseTemplate.replace('$_methodId_$', str(methodId)) \
                             .replace('$_parameterDeserializations_$', parameterDeserializations) \
                             .replace('$_methodName_$', evt.name) \
                             .replace('$_parameterList_$', evt.formatArguments(False))
            methodId += 1

        dispatchMethod = dispatchMethodTemplate.replace('$_className_$', iface.name) \
                         .replace('$_dispatchCases_$', dispatchCases)

        classMethods += classMethodsTemplate.replace('$_sendMethods_$', sendMethods) \
                        .replace('$_dispatchMethod_$', dispatchMethod)

    implFile = implFileTemplate.replace('$_headerFileName_$', f[:-3] + 'h') \
               .replace('$_classMethods_$', classMethods)
    open(f, 'w').write(cleanEmptyLines(implFile))

generateHeaders('clientproto.h')
generateHeaders('serverproto.h', True)
generateImpls('clientproto.cpp')
generateImpls('serverproto.cpp', True)
