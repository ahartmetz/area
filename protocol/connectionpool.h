/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#ifndef CONNECTIONPOOL_H
#define CONNECTIONPOOL_H

#include "eventloop.h"

#include <deque>

class TrunkConnection;

// This class is for use in the server process where it keeps track of client connections and
// global objects
class ConnectionPool : public EventLoop
{
public:
    ConnectionPool();
    ~ConnectionPool();

    // Start listening to incoming connections on local UNIX socket socketFile.
    bool listen(const char *socketFilePath);
    // Take the next incoming connection; returns null if no connection is pending
    Connection *takeIncomingConnection();

    // This connections sends each message to all connections registered with the pool
    // (one to many) and also dispatches incoming messages for global objects (one to one).
    // Connect the global objects on the server side to it.
    TrunkConnection *trunkConnection();

private:
    friend class TrunkConnection; // needs access to m_connections from EventLoop
    // reimplementations from EventLoop
    void notifyRead(int fd);
    void notifyExcept(int fd);
    void addToPollSet(Connection *connection);
    void removeFromPollSet(Connection *connection);

    void acceptConnection(); // accept an incoming connection

    int m_bindFd; // file descriptor of the listening socket that accepts new connections

    TrunkConnection *m_trunkConnection;
    std::deque<int> m_incomingConnections;
};


#endif // CONNECTIONPOOL_H
