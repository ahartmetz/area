/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#include "connectionpool.h"
#include "connection.h"
#include "trunkconnection.h"

#include <fcntl.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <cassert>
#include <cstdio>
#include <cstring>

ConnectionPool::ConnectionPool()
 : m_bindFd(-1),
   m_trunkConnection(new TrunkConnection(this))
{
}

ConnectionPool::~ConnectionPool()
{
    delete m_trunkConnection;
    close(m_bindFd);
}

bool ConnectionPool::listen(const char *socketFilePath)
{
    const int fd = socket(PF_UNIX, SOCK_STREAM, 0);
    if (fd < 0) {
        return false;
    }
    // don't let forks inherit the file descriptor - that can cause confusion...
    fcntl(fd, F_SETFD, FD_CLOEXEC);

    struct sockaddr_un addr;
    addr.sun_family = PF_UNIX;
    bool ok = strlen(socketFilePath) < sizeof(addr.sun_path);
    if (ok) {
        strcpy(addr.sun_path, socketFilePath);
    }

    unlink(socketFilePath);
    ok = ok && (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) == 0);
    ok = ok && (::listen(fd, /* max queued incoming connections */ 64) == 0);

    // add to poll set to get notified of incoming connections
    struct epoll_event epevt;
    epevt.events = EPOLLIN;
    epevt.data.u64 = 0;
    epevt.data.fd = fd;
    ok = ok && (epoll_ctl(m_epollFd, EPOLL_CTL_ADD, fd, &epevt) == 0);

    if (ok) {
        m_bindFd = fd;
    } else {
        close(fd);
    }

    return ok;
}

Connection *ConnectionPool::takeIncomingConnection()
{
    if (m_incomingConnections.empty()) {
        return 0;
    }
    const int fd = m_incomingConnections[0];
    m_incomingConnections.pop_front();
    return new Connection(fd);
}

void ConnectionPool::notifyRead(int fd)
{
    if (m_bindFd >= 0 && m_bindFd == fd) {
        acceptConnection();
    } else {
        EventLoop::notifyRead(fd);
    }
}

void ConnectionPool::acceptConnection()
{
    int connFd = accept(m_bindFd, 0, 0);
    if (connFd < 0) {
        return;
    }
    fcntl(connFd, F_SETFD, FD_CLOEXEC);

    m_incomingConnections.push_back(connFd);
}

void ConnectionPool::notifyExcept(int fd)
{
    if (m_bindFd >= 0 && m_bindFd == fd) {
        printf("ConnectionPool::notifyExcept(): What happened to m_bindFd %d?.\n", fd);
    } else {
        EventLoop::notifyExcept(fd);
    }
}

void ConnectionPool::addToPollSet(Connection *connection)
{
    const int connFd = connection->fileDescriptor();
    assert(connFd >= 0);
    if (m_connections.count(connFd) == 0) {
        m_trunkConnection->notifyAddConnection(connection);
    }
    EventLoop::addToPollSet(connection);
}

void ConnectionPool::removeFromPollSet(Connection *connection)
{
    const int connFd = connection->fileDescriptor();
    assert(connFd >= 0);
    if (m_connections.count(connFd) != 0) {
        m_trunkConnection->notifyRemoveConnection(connection);
    }
    EventLoop::removeFromPollSet(connection);
}


TrunkConnection *ConnectionPool::trunkConnection()
{
    return m_trunkConnection;
}
