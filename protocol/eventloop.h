/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#ifndef EVENTLOOP_H
#define EVENTLOOP_H

#include "connection.h"

#include <deque>

// This class's main task is notifying Connections about incoming data if / when no other event
// loop is available. On the client side this should normally only happen in small testcases.

class EventLoop
{
public:
    EventLoop();
    ~EventLoop();

    // Call this to kick off I/O either:
    // - when idle in a standalone application's main loop
    // - when select(), poll(),... has signalled read availability on pollFileDescriptor()
    // wait for timeout milliseconds, indefinitely if timeout is -1
    void poll(int timeout = -1);

    // Return pointer to the first in the queue of broken connections if any, null otherwise.
    // The pointer is removed from the queue.
    Connection *takeBrokenConnection();

    // Return the epoll (maybe later kqueue) file descriptor so an external event loop can
    // select(), poll(), epoll_wait()... on it. This file descriptor will, at least on Linux where
    // epoll is used internally, signal read availability when any file descriptor in the epoll set
    // is ready.
    int pollFileDescriptor() const;

protected:
    friend class Connection; // they are mutual friends
    virtual void addToPollSet(Connection *connection);
    virtual void removeFromPollSet(Connection *connection);

    virtual void notifyRead(int fd); // an fd has incoming data
    virtual void notifyExcept(int fd); // an "exception" (error) has occurred on fd

    int m_epollFd; // the epoll "object"

    std::map<int, Connection *> m_connections;
    std::deque<Connection*> m_brokenConnections;
};

#endif // EVENTLOOP_H