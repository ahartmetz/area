/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#include "trunkconnection.h"

#include "connectionpool.h"
#include "serverproto.h"

#include <cassert>

bool TrunkConnection::send(const Message &message)
{
    if (m_currentNewConnection) {
        m_currentNewConnection->send(message);
    } else {
        std::map<int, Connection *>::iterator it = m_pool->m_connections.begin();
        for (; it != m_pool->m_connections.end(); ++it) {
            it->second->send(message);
        }
    }
    return true;
}

void TrunkConnection::attachObject(int objectId, RemoteObject *object)
{
    std::map<int, GlobalObjectRecord>::iterator it = m_globalObjects.find(objectId);
    assert(it == m_globalObjects.end());
    m_globalObjects[objectId] = object;

    // initial state of an object is attached to all connections
    std::map<int, Connection *>::iterator cIt = m_pool->m_connections.begin();
    for (; cIt != m_pool->m_connections.end(); ++cIt) {
        it->second.receivingRemotes.insert(cIt->second);
    }

    // display does not need to be announced (and it can't be announced without a display)
    if (objectId == ObjectIdIssuance::DisplayId) {
        return;
    }

    // announce the object via display
    it = m_globalObjects.find(ObjectIdIssuance::DisplayId);
    assert(it != m_globalObjects.end());
    display *dis = dynamic_cast<display *>(it->second.object);

    dis->send_global(objectId, object->interfaceName(), 1);
}

bool TrunkConnection::maybeFinishDetaching(std::map<int, GlobalObjectRecord>::iterator *objRecord)
{
    std::map<int, GlobalObjectRecord>::iterator &it = *objRecord;
    assert(it != m_globalObjects.end());
    if (it->second.isLocalDetached && it->second.nonReceivingRemotes.empty()) {
        assert(it->second.receivingRemotes.empty()); // see bcDetachObject()
        it->second.object->notifyRemoteDetached();
        m_globalObjects.erase(it++);
        return true;
    }
    return false;
}

void TrunkConnection::detachObject(int objectId)
{
    std::map<int, GlobalObjectRecord>::iterator it = m_globalObjects.find(objectId);
    assert(it != m_globalObjects.end());

    assert(!it->second.isLocalDetached);
    it->second.isLocalDetached = true;
    it->second.nonReceivingRemotes.insert(it->second.receivingRemotes.begin(),
                                          it->second.receivingRemotes.end());
    it->second.receivingRemotes.clear();

    if (!it->second.nonReceivingRemotes.empty()) {
        Message detachMessage;
        detachMessage.beginLoad(objectId, Message::DetachMethodId);
        detachMessage.endLoad();
        std::set<Connection *>::iterator cIt = it->second.nonReceivingRemotes.begin();
        while (cIt != it->second.nonReceivingRemotes.end()) {
            if ((*cIt)->send(detachMessage)) {
                ++cIt;
            } else {
                // we won't receive a detach confirmation
                it->second.nonReceivingRemotes.erase(cIt++);
            }
        }
    }

    maybeFinishDetaching(&it);
}

int TrunkConnection::pickNewObjectId()
{
    while (true) {
        int id = ObjectIdIssuance::randomIdIn(ObjectIdIssuance::GlobalRange);
        if (!m_globalObjects.count(id)) {
            return id;
        }
    }
}

RemoteObject *TrunkConnection::objectFromId(int objectId) const
{
    std::map<int, GlobalObjectRecord>::const_iterator it = m_globalObjects.find(objectId);
    assert(it != m_globalObjects.end());
    return it->second.object;
}

static void move(Connection *what, std::set<Connection *> *from, std::set<Connection *> *to)
{
    std::set<Connection *>::iterator fromIt = from->find(what);
    if (fromIt == from->end()) {
        // must be in one container
        assert(to->count(what) == 1);
        return;
    }
    from->erase(fromIt);
    // must not be in both
    assert(to->count(what) == 0);
    to->insert(what);
}

void TrunkConnection::dispatchMessage(Connection *source, Message &message)
{
    std::map<int, GlobalObjectRecord>::iterator it = m_globalObjects.find(message.objectId());
    assert(it != m_globalObjects.end());

    if (message.methodId() == Message::DetachMethodId) {
        int removedCount = it->second.receivingRemotes.erase(source);
        removedCount += it->second.nonReceivingRemotes.erase(source);
        assert(removedCount == 0 || removedCount == 1);

        if (removedCount == 1) {
            Message detachMessage;
            detachMessage.beginLoad(message.objectId(), Message::DetachMethodId);
            detachMessage.endLoad();
            source->send(detachMessage);
        }

        maybeFinishDetaching(&it);
        return;
    }

    // accept nothing but detach messages to locally detached objects
    if (it->second.isLocalDetached) {
        return;
    }

    if (message.methodId() == Message::ChokeMethodId) {
        message.beginUnload();
        const bool isChoked = message.take<int>() != 0;
        message.endUnload();

        if (isChoked) {
            move(source, &it->second.receivingRemotes, &it->second.nonReceivingRemotes);
        } else {
            move(source, &it->second.nonReceivingRemotes, &it->second.receivingRemotes);
        }
        return;
    }

    it->second.object->dispatch(message);
}

void TrunkConnection::notifyAddConnection(Connection *connection)
{
    // announce global objects
    std::map<int, GlobalObjectRecord>::iterator it = m_globalObjects.find(ObjectIdIssuance::DisplayId);

    // no display here is not really an error if the display is created as the first global object
    // later, so it is around in time to announce any other globals. let's be strict.
    assert(it != m_globalObjects.end());
    display *const dis = dynamic_cast<display *>(it->second.object);

    for (it = m_globalObjects.begin(); it != m_globalObjects.end(); ++it) {
        it->second.receivingRemotes.insert(connection);
        if (it->first == ObjectIdIssuance::DisplayId || it->second.isLocalDetached) {
            continue;
        }
        // make the TrunkConnection send to the right connection only using a small hack...
        m_currentNewConnection = connection;
        dis->send_global(it->first, it->second.object->interfaceName(), /* TODO interface version */ 1);
        m_currentNewConnection = 0;
    }
}

void TrunkConnection::notifyRemoveConnection(Connection *connection)
{
    std::map<int, GlobalObjectRecord>::iterator it = m_globalObjects.begin();
    while (it != m_globalObjects.end()) {
        it->second.receivingRemotes.erase(connection);
        it->second.nonReceivingRemotes.erase(connection);
        if (!maybeFinishDetaching(&it)) {
            ++it;
        }
    }
}
