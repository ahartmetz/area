/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#include "connection.h"
#include "remoteobject.h"

RemoteObject::RemoteObject(IConnection *connection, int id, const char *interfaceName)
 : m_connection(connection),
   m_interfaceName(interfaceName),
   m_id(id),
   m_isDetaching(false)
{
    if (!m_id) {
        // generate one; we seem to be on the side requesting creation of the object
        m_id = m_connection->pickNewObjectId();
    }
    m_connection->attachObject(m_id, this);
}

RemoteObject::~RemoteObject()
{
    detach();
}

void RemoteObject::detach()
{
    if (m_connection && !m_isDetaching) {
        m_isDetaching = true;
        m_connection->detachObject(m_id);
        m_isDetaching = false;
        m_connection = 0;
        m_id = 0;
    }
}

void RemoteObject::notifyRemoteDetached()
{
    m_isDetaching = true;
    remoteDetached();
    m_isDetaching = false;
    m_connection = 0;
    m_id = 0;
}
