/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#include "eventloop.h"

#include <sys/epoll.h>
#include <unistd.h>

#include <cassert>
#include <cstdio>

EventLoop::EventLoop()
 : m_epollFd(epoll_create(10))
{
}

EventLoop::~EventLoop()
{
    close(m_epollFd);
}

void EventLoop::poll(int timeout)
{
    static const int maxEvPerPoll = 8;
    struct epoll_event results[maxEvPerPoll];
    int nresults = epoll_wait(m_epollFd, results, maxEvPerPoll, timeout);
    if (nresults < 0) {
        // error
        return;
    }

    for (int i = 0; i < nresults; i++) {
        struct epoll_event *evt = results + i;
        if ((evt->events & EPOLLERR) || (evt->events & EPOLLHUP)) {
            notifyExcept(evt->data.fd);
        }
        if (evt->events & EPOLLIN) {
            notifyRead(evt->data.fd);
        }
    }
}

Connection *EventLoop::takeBrokenConnection()
{
    if (m_brokenConnections.empty()) {
        return 0;
    }
    Connection *ret = m_brokenConnections[0];
    m_brokenConnections.pop_front();
    return ret;
}

int EventLoop::pollFileDescriptor() const
{
    return m_epollFd;
}

void EventLoop::addToPollSet(Connection *connection)
{
    const int connFd = connection->fileDescriptor();
    assert(connFd >= 0);
    m_connections[connFd] = connection;

    struct epoll_event epevt;
    epevt.events = EPOLLIN;
    epevt.data.u64 = 0;
    epevt.data.fd = connFd;
    epoll_ctl(m_epollFd, EPOLL_CTL_ADD, connFd, &epevt);
}

void EventLoop::removeFromPollSet(Connection *connection)
{
    const int connFd = connection->fileDescriptor();
    // the assertion is technically not necessary because Connection should call us *before*
    // resetting its fd on failure, and we don't need the fd to remove the Connection.
    assert(connFd >= 0);
    // connFd will be removed from the epoll set automatically when it is closed - if there are
    // no copies of it made using e.g. dup(). better safe than sorry anyway...
    epoll_ctl(m_epollFd, EPOLL_CTL_DEL, connFd, 0);
    m_connections.erase(connFd);
}

void EventLoop::notifyRead(int fd)
{
    std::map<int, Connection *>::iterator it = m_connections.find(fd);
    if (it != m_connections.end()) {
        it->second->notifyRead();
    } else {
        printf("ConnectionPool::notifyRead(): unhandled file descriptor %d.\n", fd);
    }
}

void EventLoop::notifyExcept(int fd)
{
    std::map<int, Connection *>::iterator it = m_connections.find(fd);
    if (it != m_connections.end()) {
        m_brokenConnections.push_back(it->second);
        //### removes the connection from m_connections
        it->second->notifyExcept();
    } else {
        printf("ConnectionPool::notifyExcept(): unhandled file descriptor %d.\n", fd);
    }
}
