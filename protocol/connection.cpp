/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#include "connection.h"
#include "connectionpool.h"
#include "remoteobject.h"
#ifdef SERVER_SIDE
#include "trunkconnection.h"
#endif

#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include "sys/uio.h"
#include <sys/un.h>
#include <unistd.h>

#include <cassert>
#include <climits>
#include <cstdlib>
#include <cstring>


// can't have class-static non-integer-type data - yes, a fixed size int array is not "integer type"
// note that id zero is invalid
static const int objIdRanges[3][2] = { {1, INT_MAX}, {INT_MIN, -1000000}, {-999999, -1} };

bool ObjectIdIssuance::isIn(Range range, int id)
{
    return id >= objIdRanges[range][0] && id <= objIdRanges[range][1];
}

int ObjectIdIssuance::randomIdIn(Range range)
{
    const int base = objIdRanges[range][0];
    const int span = objIdRanges[range][1] - base;
    const double scaleFactor = double(span) / double(INT_MAX);
    return base + int(double(random()) * scaleFactor);
}


Connection::Connection(int fd)
 : m_fd(fd),
   m_eventLoop(0),
   m_isDispatching(false)
{}

Connection::Connection(const char *socketFilePath)
 : m_fd(-1),
   m_eventLoop(0),
   m_isDispatching(false)
{
    const int fd = socket(PF_UNIX, SOCK_STREAM, 0);
    if (fd < 0) {
        return;
    }
    // don't let forks inherit the file descriptor - that can cause confusion...
    fcntl(fd, F_SETFD, FD_CLOEXEC);

    struct sockaddr_un addr;
    addr.sun_family = PF_UNIX;
    bool ok = strlen(socketFilePath) < sizeof(addr.sun_path);
    if (ok) {
        strcpy(addr.sun_path, socketFilePath);
    }

    ok = ok && (connect(fd, (struct sockaddr *)&addr, sizeof(addr)) == 0);

    if (ok) {
        m_fd = fd;
    } else {
        close(fd);
    }
}

Connection::~Connection()
{
    closeFd();
}

void Connection::addToEventLoop(EventLoop *loop)
{
    assert(!m_eventLoop);
    loop->addToPollSet(this);
    m_eventLoop = loop;
}

void Connection::removeFromEventLoop()
{
    if (m_eventLoop) {
        m_eventLoop->removeFromPollSet(this);
    }
    m_eventLoop = 0;
}

void Connection::closeFd()
{
    removeFromEventLoop();
    if (m_fd >= 0) {
        close(m_fd);
    }
    m_fd = -1;

    // iterator advances due to successive deletion of map entries
    while (!m_objectIdMap.empty()) {
        // a failed connection is an outside event that caused detachment, similar to the remote
        // object asking for detachment. pretend that we received a detach message.
        Message detachMessage;
        detachMessage.beginLoad(m_objectIdMap.begin()->first, Message::DetachMethodId);
        detachMessage.endLoad();
        dispatchMessage(detachMessage);
    }
}

bool Connection::send(const Message &message)
{
    if (m_fd < 0) {
        return false;
    }

    // sendmsg  boilerplate
    struct msghdr send_msg; // C convention makes them stand out (too many similar variables...)
    struct iovec iov;

    send_msg.msg_name = 0;
    send_msg.msg_namelen = 0;
    send_msg.msg_flags = 0;
    send_msg.msg_iov = &iov;
    send_msg.msg_iovlen = 1;

    iov.iov_base = reinterpret_cast<void *>(const_cast<char *>(message.m_buffer)); // le sigh
    iov.iov_len = message.length();

    // we can only send a fixed number of fds anyway due to the non-flexible size of the control message
    // receive buffer, so we set the same arbitrary limit of Message::m_maxTransferFds on the sending side.
    const int numFds = message.m_fileDescriptors.size();
    assert(numFds <= Message::m_maxTransferFds);

    char cmsgBuf[CMSG_SPACE(sizeof(int) * Message::m_maxTransferFds)];

    if (numFds) {
        // fill in a control message
        send_msg.msg_control = cmsgBuf;
        send_msg.msg_controllen = CMSG_SPACE(sizeof(int) * numFds);

        struct cmsghdr *c_msg = CMSG_FIRSTHDR(&send_msg);
        c_msg->cmsg_len = CMSG_LEN(sizeof(int) * numFds);
        c_msg->cmsg_level = SOL_SOCKET;
        c_msg->cmsg_type = SCM_RIGHTS;

        // set the control data to pass - this is why we don't use the simpler write()
        for (int i = 0; i < numFds; i++) {
            reinterpret_cast<int *>(CMSG_DATA(c_msg))[i] = message.m_fileDescriptors[i];
        }
    } else {
        // no file descriptor to send, no control message
        send_msg.msg_control = 0;
        send_msg.msg_controllen = 0;
    }

    while (iov.iov_len > 0) {
        int nbytes = sendmsg(m_fd, &send_msg, 0);
        if (nbytes < 0) {
            if (errno == EINTR) {
                continue;
            } else {
                closeFd();
                return false;
            }
        }

        iov.iov_base = static_cast<char *>(iov.iov_base) + nbytes;
        iov.iov_len -= nbytes;
        // sendmsg() should always return the number of bytes asked for or block, so...
        if (nbytes != 0) {
            assert(iov.iov_len == 0);
        }
    }

    return true;
}

void Connection::attachObject(int objectId, RemoteObject *object)
{
    std::map<int, RemoteObjectRecord>::iterator it = m_objectIdMap.find(objectId);
    assert(it == m_objectIdMap.end());
    m_objectIdMap[objectId] = object;
}

void Connection::detachObject(int objectId)
{
    std::map<int, RemoteObjectRecord>::iterator it = m_objectIdMap.find(objectId);
    assert(it != m_objectIdMap.end());

    if (!it->second.detachSent) {
        it->second.detachSent = true;
        Message detachMessage;
        detachMessage.beginLoad(objectId, Message::DetachMethodId);
        detachMessage.endLoad();

        if (!send(detachMessage)) {
            // prevent leak - we won't receive a detach confirmation
            it->second.detachReceived = true;
        }
    }

    if (it->second.detachSent && it->second.detachReceived) {
        // this object is gone for the purposes of the protocol
        m_objectIdMap.erase(it);
    }
}

int Connection::pickNewObjectId()
{
#ifdef SERVER_SIDE
    const ObjectIdIssuance::Range range = ObjectIdIssuance::ServerRange;
#else
    const ObjectIdIssuance::Range range = ObjectIdIssuance::ClientRange;
#endif
    while (true) {
        int id = ObjectIdIssuance::randomIdIn(range);
        if (!m_objectIdMap.count(id)) {
            return id;
        }
    }
}

RemoteObject *Connection::objectFromId(int objectId) const
{
    std::map<int, RemoteObjectRecord>::const_iterator it = m_objectIdMap.find(objectId);
    assert(it != m_objectIdMap.end());
    return it->second.object;
}

void Connection::notifyRead()
{
    int available = 0;
    do {
        doRead();
        // read until there is no more data
        if (ioctl(m_fd, FIONREAD, &available) < 0) {
            available = 0;
        }
    } while (available);
}

void Connection::doRead()
{
    // check for sentinel value to detect recursion
    assert(!m_isDispatching);

    // recvmsg-with-control-message boilerplate
    struct msghdr recv_msg;
    struct iovec iov;

    char cmsgBuf[CMSG_SPACE(sizeof(int) * Message::m_maxTransferFds)];
    memset(cmsgBuf, 0, sizeof(cmsgBuf));

    recv_msg.msg_control = cmsgBuf;
    recv_msg.msg_controllen = sizeof(cmsgBuf);
    recv_msg.msg_name = 0;
    recv_msg.msg_namelen = 0;
    recv_msg.msg_flags = 0;
    recv_msg.msg_iov = &iov;
    recv_msg.msg_iovlen = 1;

    // end boilerplate

    // first get the message length, an int

    Message message;

    int readBytes = 0;
    int messageLength = -1;
    const int headerLen = Message::m_headerSize;
    while (messageLength < 0) {
        int nbytes = recv(m_fd, message.m_buffer + readBytes, headerLen - readBytes, 0);
        if (nbytes < 0) {
            if (errno == EINTR) {
                continue;
            } else {
                closeFd();
                return;
            }
        }
        readBytes += nbytes;
        if (readBytes == headerLen) {
            messageLength = reinterpret_cast<int *>(message.m_buffer)[1] >> 16;
            assert(messageLength >= headerLen);
            assert(messageLength <= Message::m_bufSize);
        }
    }

    iov.iov_base = message.m_buffer + headerLen;
    iov.iov_len = messageLength - headerLen;
    while (iov.iov_len > 0) {
        int nbytes =  recvmsg(m_fd, &recv_msg, 0);
        if (nbytes < 0 && errno != EINTR) {
            closeFd();
            return;
        }
        iov.iov_base = static_cast<char *>(iov.iov_base) + nbytes;
        iov.iov_len -= nbytes;
        // recvmsg() should always return the number of bytes asked for or block, so...
        if (nbytes != 0) {
            assert(iov.iov_len == 0);
        }
    }

    message.m_bufferFill = messageLength; // message is in a consistent state now

    // done reading "regular data", now read any file descriptors passed via control messages

    struct cmsghdr *c_msg = CMSG_FIRSTHDR(&recv_msg);
    if (c_msg && c_msg->cmsg_level == SOL_SOCKET && c_msg->cmsg_type == SCM_RIGHTS) {
        const int len = c_msg->cmsg_len / sizeof(int);
        int *data = reinterpret_cast<int *>(CMSG_DATA(c_msg));
        for (int i = 0; i < len; i++) {
            message.appendFileDescriptor(data[i]);
        }
    }

#ifdef SERVER_SIDE
    if (ObjectIdIssuance::isIn(ObjectIdIssuance::GlobalRange, message.objectId())) {
        // global objects have  a separate id space managed (on the server) by a BroadcastConnection.
        // on the client side there is always a one-to-one RemoteObject - Connection relation that
        // requires no special handling.
        ConnectionPool *cp = dynamic_cast<ConnectionPool *>(m_eventLoop);
        cp->trunkConnection()->dispatchMessage(this, message);
        return;
    }
#endif

    dispatchMessage(message);
}

void Connection::dispatchMessage(Message &message)
{
    assert(!m_isDispatching);

    std::map<int, RemoteObjectRecord>::iterator it = m_objectIdMap.find(message.objectId());
    assert(it != m_objectIdMap.end());

    if (message.methodId() == Message::DetachMethodId) {
        // the sender has to take care not to send it twice
        assert(!it->second.detachReceived);
        it->second.detachReceived = true;
        // detachObject() can invalidate the iterator
        RemoteObject *object = it->second.object;
        const bool wasDetachSent = it->second.detachSent;
        detachObject(message.objectId());

        if (!wasDetachSent) {
            // detach initiated by remote side
            m_isDispatching = true;
            object->notifyRemoteDetached();
            m_isDispatching = false;
        }
        return;
    }

    m_isDispatching = true;
    it->second.object->dispatch(message);
    m_isDispatching = false;
}

void Connection::notifyExcept()
{
    closeFd();
}
