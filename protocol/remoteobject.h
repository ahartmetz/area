/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#ifndef REMOTEOBJECT_H
#define REMOTEOBJECT_H

class IConnection;
class Message;

// base class for autogenerated protocol-level interface classes
class RemoteObject
{
public:
    RemoteObject(IConnection *connection, int id, const char *interfaceName);
    virtual ~RemoteObject();

    IConnection *connection() const { return m_connection; }
    int objectId() const { return m_id; }

    // Dissociate from the connection; useful to "make an object disappear" without deleting it
    // immediately, e.g. if it has some data that's still needed.
    // Unregisters the object ID and sets it to zero and sets the connection() to null.
    void detach();

    const char *interfaceName() const { return m_interfaceName; }

protected:
    // Called when the object on the remote side has been detached; when this is called the object
    // has already been automatically detached on the local side, too.
    virtual void remoteDetached() {}

    // Called from Connection when a message has been received
    virtual void dispatch(Message &message) = 0;

private:
    friend class Connection;
    friend class TrunkConnection;
    void notifyRemoteDetached();

    RemoteObject(); // not implemented
    IConnection *m_connection;
    const char *m_interfaceName;
    int m_id;
    bool m_isDetaching;
};


// TODO this is only for client use; put it into an extra header and also change codegen so that
//      client-side representations of global objects inherit from it.
class RemoteGlobalObject : public RemoteObject
{
public:
    RemoteGlobalObject(IConnection *connection, int id, const char *interfaceName)
        : RemoteObject(connection, id, interfaceName) {}

    void setChokeRemote(bool);
};

#endif // REMOTEOBJECT_H