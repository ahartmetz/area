/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#include "connection.h"
#include "message.h"
#include "remoteobject.h"

#include <cassert>
#include <cstring>

// align a char offset to the next full integer size
static int alignUp(int len)
{
    const int maxStepUp = sizeof(int) - 1;
    return (len + maxStepUp) & ~maxStepUp;
}

Message::Message()
 : m_bufferFill(0),
   m_unloadPos(0)
{
#ifndef NDEBUG
    memset(m_buffer, 0xff, m_bufSize);
#endif
}

/*
 * message layout:
 * int length
 * int objectId
 * int messageId
 * (optional parameters)
 */
void Message::beginLoad(int objectId, int methodId)
{
    assert(m_bufferFill == 0);
    assert(methodId >= 0 && methodId <= 0xffff);
    reinterpret_cast<int *>(m_buffer)[0] = objectId;
    reinterpret_cast<int *>(m_buffer)[1] = methodId;
    m_bufferFill = m_headerSize;
}

void Message::endLoad()
{
    // a call with no method parameters contains three ints
    assert(m_bufferFill >= m_headerSize);
    // fill in length field in message header
    reinterpret_cast<int *>(m_buffer)[1] |= m_bufferFill << 16;
}

void Message::beginUnload()
{
    m_unloadPos = m_buffer + m_headerSize;
}

void Message::endUnload()
{
    assert(m_unloadPos == m_buffer + length());
}

void Message::addData(const void *buf, int length)
{
    // note that we are only aligning the end-of-buffer pointer, not the length for memcpy
    // the uninitialized bytes will / should never be used by anything
    const int newFill = m_bufferFill + alignUp(length);
    assert(newFill <= m_bufSize);
    memcpy(m_buffer + m_bufferFill, buf, length);
    m_bufferFill = newFill;
}

template<> void Message::append<int>(int i)
{
    addData(&i, sizeof(i));
}

template<> void Message::append<uint>(uint i)
{
    addData(&i, sizeof(i));
}

template<> void Message::append<array>(array a)
{
    addData(&a.size, sizeof(a.size));
    addData(a.data, a.size);
}

template<> void Message::append<const char *>(const char *s)
{
    const int len = strlen(s) + 1;
    addData(&len, sizeof(len));
    addData(s, len);
}

template<> void Message::append<const RemoteObject *>(const RemoteObject *obj)
{
    const int id = obj->objectId();
    addData(&id, sizeof(id));
}

void Message::appendFileDescriptor(int fd)
{
    m_fileDescriptors.push_back(fd);
}

template<> int Message::take()
{
    int ret = *reinterpret_cast<int *>(m_unloadPos);
    m_unloadPos += sizeof(ret);
    return ret;
}

template<> uint Message::take()
{
    uint ret = *reinterpret_cast<uint *>(m_unloadPos);
    m_unloadPos += sizeof(ret);
    return ret;
}

template<> array Message::take()
{
    array ret;
    ret.size = *reinterpret_cast<int *>(m_unloadPos);
    m_unloadPos += sizeof(ret.size);
    ret.data = m_unloadPos;
    m_unloadPos += alignUp(ret.size);
    return ret;
}

template<> const char *Message::take()
{
    int len = *reinterpret_cast<int *>(m_unloadPos);
    m_unloadPos += sizeof(len);
    const char *ret = m_unloadPos;
    m_unloadPos += alignUp(len);
    return ret;
}


int Message::fileDescriptor(int index) const
{
    // this should implicitly assert that the vector is long enough
    return m_fileDescriptors[index];
}
