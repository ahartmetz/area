/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#ifndef CONNECTION_H
#define CONNECTION_H

#include "message.h"

#include <map>

class IConnection;
class Message;
class ConnectionPool;


class ObjectIdIssuance
{
public:
    // there are three ranges: for objects created on client initiative and for objects created
    // on server initiative; keeping those separate makes collision avoidance easy.
    // the third range is for global objects like the display where one object appears on all
    // connections, so keeping other ids out of that range again simplifies bookkeeping.
    enum Range {
        ClientRange = 0,
        ServerRange,
        GlobalRange
    };

    enum {
        DisplayId = -1 // to bootstrap connections
    };
    static bool isIn(Range range, int id);
    static int randomIdIn(Range range);
};

class IConnection
{
public:
    virtual ~IConnection() {}

    virtual bool isValid() const = 0;
    virtual bool send(const Message &message) = 0;

    virtual void attachObject(int objectId, RemoteObject *object) = 0;
    virtual void detachObject(int objectId) = 0;

    virtual int pickNewObjectId() = 0; // pick a free object ID
    virtual RemoteObject *objectFromId(int objectId) const = 0;
};

class EventLoop;
class ConnectionPool;

class Connection : public IConnection
{
public:
    // Connect to local socket at socketFilePath
    Connection(const char *socketFilePath);

    ~Connection();

    int fileDescriptor() const { return m_fd; }

    // pure virtuals from IConnection
    bool isValid() const { return m_fd >= 0; }

    bool send(const Message &message);

    void attachObject(int objectId, RemoteObject *object);

    // both of the following methods need to be called to remove an object for good
    // call this when the object is gone on our side
    void detachObject(int objectId);

    int pickNewObjectId();
    RemoteObject *objectFromId(int objectId) const;
    // end IConnection

    void addToEventLoop(EventLoop *loop);
    EventLoop *eventLoop() const { return m_eventLoop; }
    void removeFromEventLoop();

private:
    friend class EventLoop;
    friend class ConnectionPool;
    // ConnectionPool uses this constructor for incoming connections
    Connection(int fd);

    Connection(); // not implemented
    Connection(const Connection &); // not implemented, disable copying
    Connection &operator=(const Connection &); // dito

    void notifyRead();
    void notifyExcept();

    void doRead();
    void dispatchMessage(Message &message);

    void closeFd();

    int m_fd;
    EventLoop *m_eventLoop;

    struct RemoteObjectRecord
    {
        RemoteObjectRecord(RemoteObject *obj = 0)
         : object(obj), detachSent(false), detachReceived(false) {}
        RemoteObject *object;
        bool detachSent : 1;
        bool detachReceived : 1;
    };
    // objectId to object mapping, with some flags
    std::map<int, RemoteObjectRecord> m_objectIdMap;

    bool m_isDispatching;
};

#endif // CONNECTION_H
