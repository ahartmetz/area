/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#include "protocol/connection.h"
#include "protocol/connectionpool.h"
#include "protocol/trunkconnection.h"
#include "serverproto.h"

#include <unistd.h>

#include <cstdio>

#include <set>

class Display : public display
{
public:
    Display(IConnection *connection, int id)
     : display(connection, id)
    {}

    // reimplement pure virtuals from display
    void receive_sync(uint key);
    void receive_frame(uint key);
};

class Output : public output
{
public:
    Output(IConnection *connection, int id = 0)
        : output(connection, id)
    {}
};

class Visual : public visual
{
public:
    Visual(IConnection *connection, int id = 0)
        : visual(connection, id)
    {}
};

void Display::receive_sync(uint key)
{
    printf("Display::receive_sync\n");
}

void Display::receive_frame(uint key)
{
    printf("Display::receive_frame\n");
}

static void doStuffWithDisplay(Display *d)
{
    d->send_invalid_object(23);
    d->send_invalid_method(24, 42);
    d->send_no_memory();
    d->send_range(10);
    d->send_key('a', 1200);
    d->send_key(101, 140);
    d->send_key('A', 1200);
}

static void doStuffWithOutput(Output *o)
{
    o->send_geometry(1, 2, 3, 4);
}

int main(int argc, const char *argv[])
{
    ConnectionPool cPool;
    cPool.listen("./areaSocket");
    std::set<Connection *> connections;
    // the "master"/bootstrap global object that can create the others on the client side;
    // it needs a fixed id to communicate with its counterpart created in the same way.

    // TODO: (in that order actually)
    // - wayland compatible protocol
    // - destruction of globals (seems to be missing from wayland, too)

    // API changes:
    // - (maybe hide ConnectionPool / EventLoop in display / Display)
    // - think about a parent / child system or other form of semi-automatic lifetime / memory management

    Display display(cPool.trunkConnection(), ObjectIdIssuance::DisplayId);
    Output output(cPool.trunkConnection());
    Visual visual(cPool.trunkConnection());
    while (true) {
        cPool.poll();
        while (Connection *conn = cPool.takeBrokenConnection()) {
            connections.erase(connections.find(conn));
            delete conn;
        }
        while (Connection *conn = cPool.takeIncomingConnection()) {
            connections.insert(conn);
            conn->addToEventLoop(&cPool);
            // the next poll() will start notifying the connection of read availability, if any
        }
        doStuffWithDisplay(&display);
        doStuffWithOutput(&output);
        usleep(850000);
    }
}
