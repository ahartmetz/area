/*
   Copyright (C) 2011 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#include "protocol/connection.h"
#include "protocol/connectionpool.h"
#include "clientproto.h"

#include <cassert>
#include <cstdio>
#include <cstring>

class Display : public display
{
public:
    Display(IConnection *connection, int id)
     : display(connection, id) {}

    // reimplement pure virtuals from display
    void receive_invalid_object(uint object_id);
    void receive_invalid_method(uint object_id, uint opcode);
    void receive_no_memory();
    void receive_global(int id, const char *name, uint version);
    void receive_range(uint base);
    void receive_key(uint key, uint time);
};

class Visual : public visual
{
public:
    Visual(IConnection *connection, int id)
     : visual(connection, id) {}
// this one has no methods. strange, huh?
};

class Output : public output
{
public:
    Output(IConnection *connection, int id)
     : output(connection, id) {}

     void receive_geometry(int x, int y, int width, int height);
};

void Display::receive_invalid_object(uint object_id)
{
    printf("Display::receive_invalid_object(uint object_id = %d)\n", object_id);
}

void Display::receive_invalid_method(uint object_id, uint opcode)
{
    printf("Display::receive_method(uint object_id = %d, uint opcode = %d)\n", object_id, opcode);
}

void Display::receive_no_memory()
{
    printf("Display::receive_no_memory()\n");
}

void Display::receive_global(int id, const char *name, uint version)
{
    printf("Display::receive_global(int id = %d, const char *name = %s, uint version = %d)\n",
           id, name, version);
    assert(ObjectIdIssuance::isIn(ObjectIdIssuance::GlobalRange, id));
    if (strcmp(name, "visual") == 0) {
        new Visual(connection(), id);
    } else if (strcmp(name, "output") == 0) {
        new Output(connection(), id);
    }
}

void Display::receive_range(uint base)
{
    // we ignore this and use a better approach
    printf("Display::receive_range(uint base = %d)\n", base);
    send_frame(131235);
}

void Display::receive_key(uint key, uint time)
{
    printf("Display::receive_key(uint key = %d, uint time = %d)\n", key, time);
    send_sync(12345678);
}

void Output::receive_geometry(int x, int y, int width, int height)
{
    printf("Output::receive_geometry(int x = %d, int y = %d, int width = %d, int height = %d)\n",
           x, y, width, height);
}

int main(int argc, const char *argv[])
{
    EventLoop evtLoop;
    Connection conn("./areaSocket");
    if (!conn.isValid()) {
        printf("could not connect to server socket at ./areaSocket\n");
        return 1;
    }
    conn.addToEventLoop(&evtLoop);
    // the next poll() will start notifying the connection of read availability, if any

    // let's pretend that Display was created on server initiative, which it is, kind of
    // most importantly it must have the same id as the Display object on the server
    Display display(&conn, ObjectIdIssuance::DisplayId);
    while (conn.isValid()) {
        evtLoop.poll();
    }
}
